package services

import (
	"gitlab.com/ardaktym/mvc/domain"
	"gitlab.com/ardaktym/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)

}
