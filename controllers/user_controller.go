package controllers

import (
	"encoding/json"
	"gitlab.com/ardaktym/mvc/services"
	"gitlab.com/ardaktym/mvc/utils"
	"net/http"
	"strconv"
)

func GetUser(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.ParseInt(r.URL.Query().Get("user_id"), 10, 64)
	if err != nil {
		apiErr := &utils.ApplicationError{
			Message: "user_id must be a number",
			Status:  http.StatusBadRequest,
			Code:    "bad_request",
		}
		jsonValue, _ := json.Marshal(apiErr)
		w.WriteHeader(apiErr.Status)
		w.Write(jsonValue)
		return
	}
	user, apiErr := services.GetUser(userId)
	if apiErr != nil {
		jsonValue, _ := json.Marshal(apiErr)
		w.WriteHeader(apiErr.Status)
		w.Write(jsonValue)
		return
	}
	jsonValue, _ := json.Marshal(user)
	w.Write(jsonValue)

}
