package domain

import (
	"gitlab.com/ardaktym/mvc/utils"
	"net/http"
)

var users = map[int64]*User{
	13: {
		Id:        13,
		FirstName: "Tleuzhan",
		LastName:  "Mukatatyev",
		Email:     "tleuzhan13@gmail.com",
	},
	12: {
		Id:        12,
		FirstName: "Imangali",
		LastName:  "Kalybek",
		Email:     "imangalikalybek@gmail.com",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) {
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: "user not found",
		Status:  http.StatusNotFound,
		Code:    "not_found",
	}

}
